
import webPages.HomePage;
import org.testng.annotations.Test;



public class Tests {

  public HomePage homePage = new HomePage();

  @Test
  public void load() {
    homePage.loadPage();
  }

  @Test(dependsOnMethods = "load")
  public void verifyLink (){
    homePage.openPageForActor();
  }
}
