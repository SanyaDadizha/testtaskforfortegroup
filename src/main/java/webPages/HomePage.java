package webPages;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import java.util.List;

public class HomePage extends BasePage {

    public static final String baseURL = "https://en.wikipedia.org/wiki/To_Kill_a_Dragon";

    @FindBy (xpath = "//span[@id='Cast']/../following-sibling::ul[1]/li/a")
    public List<WebElement> listOfCast;

    @FindBy(linkText = "To Kill a Dragon")
    public WebElement linkText;

    public static String getPageUrl() {
        return baseURL;
    }

    public void openPageForActor() {
        int listOfCastLength = listOfCast.size();

        for (int i=0; i< listOfCastLength; i++) {
            //org.openqa.selenium.StaleElementReferenceException
            List<WebElement> listOfCast2 = findElements(By.xpath("//span[@id='Cast']/../following-sibling::ul[1]/li/a"));
                    listOfCast2.get(i).click();

            implicitlyWait();
            verifyElementIsPresent(linkText);
            back();
        }

    }

}
