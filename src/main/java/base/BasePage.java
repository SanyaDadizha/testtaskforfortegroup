package base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webPages.HomePage;
import initial.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class BasePage {

    static private WebDriver driver;
    static private WebDriverWait wait;
    private static final Integer  TIMELOAD =10, IMPLICIT_WAIT = 10;


    {
        if(driver == null)
            driver = BrowserFactory.startBrowser();
        wait = new WebDriverWait(driver, TIMELOAD);
        PageFactory.initElements(driver, this);
    }

    public void loadPage() {
        driver.manage().window().maximize();
        driver.get(HomePage.getPageUrl());
    }

    public WebElement waitForElementToBeClickable(WebElement element){
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public List<WebElement> waitForElements(WebElement el){
        return wait.until(ExpectedConditions.visibilityOfAllElements(el));
    }

    public  void implicitlyWait(){
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
    }

    public List<WebElement> findElements(By element) {
        return driver.findElements(element);
    }

    public void clickElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

    public boolean verifyElementIsPresent(WebElement element) {
        try {
            element.getTagName();
            System.out.println(element.getTagName());
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    public void back() { driver.navigate().back();}
    public void refresh(){
        driver.navigate().refresh();
    }
}
